@extends('masterlayout')


@section('analysergridreviewtext')
<script>
    function grider (){
        mygrid = new dhtmlXGridObject('gridbox_reviews_text');
        mygrid.setImagePath("./codebase/imgs/");
        mygrid.setHeader("Reviews");
        mygrid.setInitWidths("*");
        mygrid.setColAlign("left");
        mygrid.setColTypes("ro");
        mygrid.setColSorting("str");
        mygrid.init();
        mygrid.load("connector_review_text.php");

    }
</script>
@stop

@section('analysergridword')
    <script>
        function grider2 (){
            mygrid = new dhtmlXGridObject('gridbox_extract_keywords');
            mygrid.setImagePath("./codebase/imgs/");
            mygrid.setHeader("Words");
            mygrid.setInitWidths("*");
            mygrid.setColAlign("left");
            mygrid.setColTypes("ro");
            mygrid.setColSorting("str");
            mygrid.init();
            mygrid.load("connector_feature_keyword_extract.php");
        }
    </script>
@stop


@section('feature-extract-words')
    <script>
        function onLoader(){
            var xmlhttp= new XMLHttpRequest();
            xmlhttp.onreadystatechange= function(){
                if(this.readyState == 4 && this.status == 200){
                    console.log("Processing data");
                }
            };
            xmlhttp.open("GET","processdata.php");
            xmlhttp.send();
        }
    </script>
@stop


