<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <link rel="stylesheet" href="materialize/materialize/css/materialize.min.css">
    <script src="materialize/materialize/js/materialize.min.js"></script>
    <link rel="STYLESHEET" type="text/css" href="dhtmlx/dhtmlxSuite/codebase/dhtmlx.css">

    <script src="dhtmlx/dhtmlxSuite/codebase/dhtmlx.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


    <title>Reviews</title>
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Analyzer</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">History</a></li>
        </ul>
    </div>
</nav>

<div>
    <div class="block">
        <div class="z-depth-5" id="gridbox_reviews_text" style="width:500px;height:400px;"></div>
        <button class="btn btn-danger navbar-btn" onclick="grider()">Load Reviews</button>
        <button class="btn btn-danger navbar-btn" onclick="onLoader()">Process Data</button>
        <div id="gridbox" style="width:500px;height:400px;"></div>
    </div>
    <div class="block">
        <div class="z-depth-5" id="gridbox_extract_keywords" style="width:500px;height:400px;" ></div>
        <button class="btn btn-danger navbar-btn" onclick="grider2()">Extract Words</button>
        <div id="gridbox" style="width:500px;height:400px;"></div>
    </div>
</div>

<style>
    .block {
        width: 860px;
        display: inline-block; // display inline with abality to provide width/height
    }​
</style>

@yield('analysergridreviewtext')
@yield('analysergridword')
@yield('feature-extract-words')


    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col l6 s12">
                    <h5 class="white-text">Online Review Clustering</h5>
                    <p class="grey-text text-lighten-4">Analyzing text data and clustering them into groups</p>
                </div>
            </div>
        </div>
    </div>

    <style>
        .main{
            background-color: #343434;
            flex: 1 0 auto;
        }
    </style>


</body>
</html>
