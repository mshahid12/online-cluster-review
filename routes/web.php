<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/analyse', function () {
    return view('analyser');
});

Route::get('/reviews','extractreviews@index');

/*

Route::get('/about',function(){
    return "This is about page";
});

Route::get('/contact',function(){
    return "This is contact page";
});

Route::get('/posts/{id}/{name}',function($id,$name){
    return "This is the id : ".$id. " and the name is : ".$name;
});
*/

//Yet to see how they work
//Route::get('admin/posts/example',array('as' => 'admin.home' ,function(){
/*    $url = route('admin.home');

    return "this is url ".$url;
}));*/


//Route::resource('/post/res/{$id}','PostsController@index');

//Route::get('post/res/{id}','PostsController@index');
